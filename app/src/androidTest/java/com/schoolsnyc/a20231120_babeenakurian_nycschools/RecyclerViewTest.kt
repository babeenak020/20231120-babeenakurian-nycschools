package com.schoolsnyc.a20231120_babeenakurian_nycschools

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.NoMatchingViewException
import androidx.test.espresso.ViewAssertion
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.assertThat
import androidx.test.espresso.matcher.ViewMatchers.hasMinimumChildCount
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import com.schoolsnyc.a20231120_babeenakurian_nycschools.ui.MainActivity
import org.hamcrest.Matchers.`is`

@RunWith(AndroidJUnit4::class)
class RecyclerViewTest {

    @get:Rule
    val activityScenarioRule = ActivityScenarioRule(MainActivity::class.java)
    @Test
    fun yourTestFunction() {
        onView(withId(R.id.recyclerView)).check(matches(isDisplayed()))
        // Assuming your RecyclerView has items displayed
        onView(withId(R.id.recyclerView)).check(matches(hasMinimumChildCount(1))) // Replace '5' with expected item count

        // Check if a particular value is present in the RecyclerView
        onView(withText("Clinton School Writers & Artists, M.S. 260")).check(matches(isDisplayed()))

    }

}